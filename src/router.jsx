import React from "react";
import {
    createBrowserRouter
  } from "react-router-dom";

import Homepage from "./layout/Homepage";
import DetailUser from "./users/DetailUser";
import ListUser from "./users/ListUser";

const router = createBrowserRouter([
    {
        path: "/",
        element: <Homepage/>,
        children: [
            {
                index:true,
                element: <ListUser/>
            },
            {
                path:"detail/:id",
                element: <DetailUser/>
            }
        ]
    }
])


export default router;