import axios from "axios";
import { React, useEffect, useState } from "react";
import { useParams, useNavigate } from "react-router-dom";

export default function DetailUser() {
  const navigate = useNavigate();
  const { id } = useParams();
  const [user, setUser] = useState(null);

  const getUser = (id) => {
    axios
      .get("https://jsonplaceholder.typicode.com/users/" + id)
      .then((response) => setUser(response.data));
  };
  useEffect(() => {
    getUser(id);
  }, []);

  return (
    <>
      <div className="flex justify-center pt-20">
        <div className="block w-1/2 rounded-lg bg-white p-6 shadow-lg dark:bg-neutral-700 p-14">
          <h5 className="mb-5 text-xl font-medium leading-tight text-neutral-800 dark:text-neutral-50">
            User Detals
          </h5>
          <div className="mb-4 text-base text-neutral-600 dark:text-neutral-200">
            <div className="grid grid-flow-col w-full mb-4">
              <div>Name</div>
              <div>{user?.name}</div>
            </div>
            <div className="grid grid-flow-col w-full mb-4">
              <div>Username</div>
              <div>{user?.username}</div>
            </div>
            <div className="grid grid-flow-col w-full mb-4">
              <div>Email</div>
              <div>{user?.email}</div>
            </div>
            <div className="grid grid-flow-col w-full mb-4">
              <div>Phone</div>
              <div>{user?.phone}</div>
            </div>
            <div className="grid grid-flow-col w-full mb-4">
              <div>Website</div>
              <div>{user?.website}</div>
            </div>
            <div className="grid grid-flow-col w-full mb-4">
              <div>Company</div>
              <div>{user?.company?.name}</div>
            </div>
            <div className="grid grid-flow-col w-full mb-4">
              <div>Address</div>
              <div>
                {user?.address?.suite}, {user?.address?.street},{" "}
                {user?.address?.city}
              </div>
            </div>
            <div className="grid grid-flow-col w-full mb-4">
              <div>Zipcode</div>
              <div>{user?.address?.zipcode}</div>
            </div>
          </div>
          <button
            type="button"
            className="inline-block rounded bg-primary px-6 pt-2.5 pb-2 text-xs font-medium uppercase leading-normal text-white shadow-[0_4px_9px_-4px_#3b71ca] transition duration-150 ease-in-out hover:bg-primary-600 hover:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:bg-primary-600 focus:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:outline-none focus:ring-0 active:bg-primary-700 active:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] mt-5"
            data-te-ripple-init
            data-te-ripple-color="light"
            onClick={() => navigate(-1)}
          >
            Back
          </button>
        </div>
      </div>
    </>
  );
}
