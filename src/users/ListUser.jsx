import axios from "axios";
import {React, useState, useEffect } from "react";
import { Link } from "react-router-dom";

export default function ListUser() {
  const [users, setUsers] = useState(null);

  const getAllUser = () => {
    axios
    .get("https://jsonplaceholder.typicode.com/users")
    .then(function (response) {
      // handle success
      setUsers(response.data);
      console.log(users);
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
    .finally(function () {
      // console.log(customers);
    });
  }
    useEffect(() => {
        getAllUser();
    }, []);

  return (
    <>
      <div className="flex flex-col mx-auto w-4/5">
        <h3 className="text-3xl my-5">List User</h3>
        <div className="overflow-x-auto sm:-mx-6 lg:-mx-8">
          <div className="inline-block min-w-full py-2 sm:px-6 lg:px-8">
            <div className="overflow-hidden">
              <table className="min-w-full text-left text-sm font-light">
                <thead className="border-b font-medium dark:border-neutral-500">
                  <tr>
                    <th scope="col" className="px-6 py-4">
                      #
                    </th>
                    <th scope="col" className="px-6 py-4">
                      Name
                    </th>
                    <th scope="col" className="px-6 py-4">
                      Username
                    </th>
                    <th scope="col" className="px-6 py-4">
                      Email
                    </th>
                    <th scope="col" className="px-6 py-4">
                      Action
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {users?.map((e,i,arr)=>(
                        <tr className="border-b dark:border-neutral-500" key={e.id}>
                            <td className="whitespace-nowrap px-6 py-4 font-medium">{i}</td>
                            <td className="whitespace-nowrap px-6 py-4">{e.name}</td>
                            <td className="whitespace-nowrap px-6 py-4">{e.username}</td>
                            <td className="whitespace-nowrap px-6 py-4">{e.email}</td>
                            <td className="whitespace-nowrap px-6 py-4">
                                <Link to={"/detail/"+e.id} className="inline-block rounded bg-info  px-6 pt-2.5 pb-2 text-xs font-medium uppercase leading-normal text-white shadow-[0_4px_9px_-4px_#3b71ca] transition duration-150 ease-in-out hover:bg-info-600 hover:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:bg-primary-600 focus:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)] focus:outline-none focus:ring-0 active:bg-primary-700 active:shadow-[0_8px_9px_-4px_rgba(59,113,202,0.3),0_4px_18px_0_rgba(59,113,202,0.2)]"
    >View</Link>
                            </td>
                      </tr>
                    )
                  )}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
